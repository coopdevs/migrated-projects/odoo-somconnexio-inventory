odoo12-addon-somconnexio==12.0.1.9.9
git+https://github.com/coopdevs/l10n-spain@12.0-account-payment-return-import-n19#egg=odoo12-addon-account-payment-return-import-n19&subdirectory=setup/account_payment_return_import_n19
git+https://github.com/coopdevs/rest-framework@feature/patched-base-rest-12-0-3-0-7#egg=odoo12-addon-base-rest&subdirectory=setup/base_rest
odoo12-addon-web-environment-ribbon==12.0.1.0.0.99.dev16
